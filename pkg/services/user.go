package services

import (
	"context"
	"fmt"
	"net/http"

	"gitlab.com/delivery-go-react/back/user-svc/pkg/db"
	"gitlab.com/delivery-go-react/back/user-svc/pkg/models"
	"gitlab.com/delivery-go-react/back/user-svc/pkg/pb"
	"gitlab.com/delivery-go-react/back/user-svc/pkg/utils"
)

type Server struct {
	H   db.Handler
	Jwt utils.JwtWrapper
}

func (s *Server) Register(ctx context.Context, req *pb.RegisterRequest) (*pb.RegisterResponse, error) {
	var user models.User

	if result := s.H.DB.Where(&models.User{Email: req.Email}).First(&user); result.Error == nil {
		return &pb.RegisterResponse{
			Status: http.StatusConflict,
			Error:  "E-Mail already exists",
		}, nil
	}

	user.Email = req.Email
	user.Password = utils.HashPassword(req.Password)
	user.RoleId = req.RoleId
	fmt.Println("user", user)

	s.H.DB.Create(&user)

	return &pb.RegisterResponse{
		Status: http.StatusCreated,
	}, nil
}

func (s *Server) Login(ctx context.Context, req *pb.LoginRequest) (*pb.LoginResponse, error) {
	var user models.User

	if result := s.H.DB.Where(&models.User{Email: req.Email}).First(&user); result.Error != nil {
		return &pb.LoginResponse{
			Status: http.StatusNotFound,
			Error:  "User not found",
		}, nil
	}

	fmt.Println(req)
	fmt.Println(user)

	match := utils.CheckPasswordHash(req.Password, user.Password)

	if !match {
		return &pb.LoginResponse{
			Status: http.StatusNotFound,
			Error:  "User not found",
		}, nil
	}

	token, _ := s.Jwt.GenerateToken(user)

	return &pb.LoginResponse{
		Status: http.StatusOK,
		Token:  token,
	}, nil
}

func (s *Server) Validate(ctx context.Context, req *pb.ValidateRequest) (*pb.ValidateResponse, error) {
	claims, err := s.Jwt.ValidateToken(req.Token)

	if err != nil {
		return &pb.ValidateResponse{
			Status: http.StatusBadRequest,
			Error:  err.Error(),
		}, nil
	}

	var user models.User

	if result := s.H.DB.Where(&models.User{Email: claims.Email}).First(&user); result.Error != nil {
		return &pb.ValidateResponse{
			Status: http.StatusNotFound,
			Error:  "User not found",
		}, nil
	}

	fmt.Println(user)

	return &pb.ValidateResponse{
		Status:    http.StatusOK,
		Error:     "",
		UserEmail: user.Email,
		UserId:    user.Id,
		RoleId:    user.RoleId,
	}, nil
}

func (s *Server) ChangePassword(ctx context.Context, req *pb.ChangePasswordRequest) (*pb.ChangePasswordResponse, error) {
	var user models.User
	var userTest models.User

	fmt.Println("req", req)

	if result := s.H.DB.Where(&models.User{Id: req.UserId}).First(&user); result.Error != nil {
		return &pb.ChangePasswordResponse{
			Status: http.StatusNotFound,
			Error:  "User not found",
		}, nil
	}

	if result := s.H.DB.Where(&models.User{Id: 1}).First(&userTest); result.Error != nil {
		return &pb.ChangePasswordResponse{
			Status: http.StatusNotFound,
			Error:  "User not found",
		}, nil
	}
	fmt.Println(userTest)

	match := utils.CheckPasswordHash(req.CurrentPassword, user.Password)

	if !match {
		return &pb.ChangePasswordResponse{
			Status: http.StatusNotFound,
			Error:  "Passwords not match",
		}, nil
	}

	user.Password = utils.HashPassword(req.NewPassword)
	fmt.Println("req", user)
	if err := s.H.DB.Save(user).Error; err != nil {
		return &pb.ChangePasswordResponse{
			Status: http.StatusBadRequest,
			Error:  "Couldn't save password",
		}, nil
	}

	return &pb.ChangePasswordResponse{
		Status: http.StatusOK,
	}, nil
}
