package main

import (
	"fmt"
	"log"
	"net"

	"gitlab.com/delivery-go-react/back/user-svc/pkg/config"
	"gitlab.com/delivery-go-react/back/user-svc/pkg/db"
	"gitlab.com/delivery-go-react/back/user-svc/pkg/pb"
	"gitlab.com/delivery-go-react/back/user-svc/pkg/services"
	"gitlab.com/delivery-go-react/back/user-svc/pkg/utils"
	"google.golang.org/grpc"
)

func main() {
	c, err := config.LoadConfig()

	if err != nil {
		log.Fatalln("Failed at config", err)
	}

	h := db.Init(c)

	jwt := utils.JwtWrapper{
		SecretKey:       c.JWTSecretKey,
		Issuer:          "user-svc",
		ExpirationHours: 24 * 365,
	}

	lis, err := net.Listen("tcp", c.Port)

	if err != nil {
		log.Fatalln("Failed to listing:", err)
	}

	fmt.Println("User Svc on", c.Port)

	s := services.Server{
		H:   h,
		Jwt: jwt,
	}

	grpcServer := grpc.NewServer()

	pb.RegisterUserServiceServer(grpcServer, &s)

	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalln("Failed to serve:", err)
	}
}
